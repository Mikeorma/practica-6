﻿using UnityEngine;

public class Spawner : MonoBehaviour {

    public GameObject objectToSpawn;

	void Update () {

        if (Input.GetKeyDown(KeyCode.S)) {

            GameObject objAux = Instantiate(objectToSpawn, transform.position, Quaternion.identity) as GameObject;
            float scale = Random.Range(1, 10);
            objAux.transform.localScale = new Vector3(scale, scale, scale);
            Rigidbody rb = objAux.GetComponent<Rigidbody>();
            rb.drag = Random.Range(0f, 0.1f);
            rb.mass = Random.Range(1, 25);
            rb.AddForce(Vector3.right * Random.Range(10, 150), ForceMode.Impulse);
        }
	}
}

