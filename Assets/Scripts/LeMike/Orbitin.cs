﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Orbitin : MonoBehaviour {

    public Transform centrodegiro;
    public GameObject objetosaliente;



    public Vector3 desiredPosition;
    public float radiodegiro = 30.0f;
    public float VelocRad = 100.0f;
    public float VelocRot = 80.0f;
    public Color color;
    public float spawntime;
    private int numero;




    void Start()
    {

        transform.position = (transform.position - centrodegiro.position).normalized * radiodegiro + centrodegiro.position;
        radiodegiro = 30.0f;
        VelocRad = 100.0f;

        

    }

    void Update()
    {
        transform.RotateAround(centrodegiro.position, Vector3.up, VelocRot * Time.deltaTime);
        desiredPosition = (transform.position - centrodegiro.position).normalized * radiodegiro + centrodegiro.position;
        transform.position = Vector3.MoveTowards(transform.position, desiredPosition, Time.deltaTime * VelocRad);

        spawn();

    }

    void spawn()
    {
        spawntime -= Time.deltaTime;
        if (spawntime <= 0)
        {
            GameObject auxiliar = Instantiate(objetosaliente, transform.position, Quaternion.identity) as GameObject;
            float escalaobj = Random.Range(1, 10);
            auxiliar.transform.localScale = new Vector3(escalaobj, escalaobj, escalaobj);
            auxiliar.GetComponent<Renderer>().material.color = color;
            spawntime = Random.Range(0.5f, 3f);
            Rigidbody bola = auxiliar.GetComponent<Rigidbody>();

            numero = Random.Range(1, 4);
            if (numero == 1)
            {
                bola.AddForce(Vector3.right * Random.Range(10, 150), ForceMode.Impulse);
            }
            else if (numero == 2)
            {
                bola.AddForce(Vector3.left * Random.Range(10, 150), ForceMode.Impulse);
            }
            else if (numero == 3)
            {
                bola.AddForce(Vector3.up * Random.Range(10, 150), ForceMode.Impulse);
            }
            else if (numero == 4)
            {
                bola.AddForce(Vector3.down * Random.Range(10, 150), ForceMode.Impulse);
            }
            
            
            
        }
    }

}
