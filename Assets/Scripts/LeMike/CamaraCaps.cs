﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamaraCaps : MonoBehaviour
{

    public float horiz = 10f;
    public float verti = 10f;

    // Update is called once per frame
    void Update()
    {
        float h = horiz * Input.GetAxis("Mouse X");
        float v = verti * Input.GetAxis("Mouse Y");
        transform.Rotate(0, h, 0);
    }
}

