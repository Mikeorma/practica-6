﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeMegaDeathRay : MonoBehaviour {

    [Range(0.01f, 1000f)]
    public float distance = 300;
    public Camera mycam;


    

	void FixedUpdate () {

        if (Input.GetMouseButton(0))
        {
            
            RaycastHit hit;
            Ray vray = mycam.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(vray, out hit, distance))
            {
                if(hit.transform.gameObject && hit.transform.gameObject.tag == "PrefabMike")
                {
                    Destroy(hit.collider.gameObject);
                    print("I hit");
                }
            }
        }
		
	}
}
