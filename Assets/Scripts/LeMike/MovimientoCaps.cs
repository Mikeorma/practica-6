﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoCaps : MonoBehaviour {

    public float velocidadcaps = 2f;
    private float horinput, verinput;
    public Rigidbody rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void FixedUpdate () {
        horinput = Input.GetAxisRaw("Horizontal");
        verinput = Input.GetAxisRaw("Vertical");



        if (verinput < 0)
        {
            rb.velocity = new Vector3(-1, 0, 0) * velocidadcaps;
        }

        if (verinput > 0)
        {
            rb.velocity = new Vector3(1, 0, 0) * velocidadcaps;
        }

        if (horinput > 0)
        {
            rb.velocity = new Vector3(0,0,-1) * velocidadcaps;
        }

        if (horinput < 0)
        {
            rb.velocity = new Vector3(0, 0, 1) * velocidadcaps;
        }

    }
}
